Attribute VB_Name = "Module3"
Option Explicit

Sub Delete(r As Integer, c As Integer)
Attribute Delete.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Macro1 Macro
'

'
'    Range(Cells(r, c - 6), Cells(r, c - 2)).Select
'    Selection.Delete Shift:=xlUp

    ActiveSheet.Cells(r, c - 4).Select
    Selection.ClearContents
    ActiveSheet.Cells(r, c - 2).Select
    Selection.ClearContents
    ActiveSheet.Cells(r, c - 1).Select
    Selection.ClearContents
End Sub

Sub Button1_Click()
    Dim btn As Button, t As Range, sht As Worksheet, i As Long

    Set sht = ActiveSheet

    sht.Buttons.Delete

    For i = 5 To sht.Cells(Rows.Count, "A").End(xlUp).Row
        Set t = sht.Cells(i, 10)
        Set btn = sht.Buttons.Add(t.Left, t.Top, t.Width, t.Height)
        With btn
          .OnAction = "Createbutton"
          .Caption = "Preparer"
          .Name = "Preparer_" & i
        End With
    Next i
   ' Call Delete(5, 9)
End Sub



Sub USER()
  Dim btn As Button
  Dim i
  Dim MacroD
  Dim j
  Application.ScreenUpdating = False
  ActiveSheet.Buttons.Delete
  Dim t As Range
  For i = 5 To 20 Step 1
  For j = 1 To 20 Step 1
    Set t = ActiveSheet.Range(Cells(i, j * 7), Cells(i, j * 7))
    Set btn = ActiveSheet.Buttons.Add(t.Left + 2.5 * j, t.Top, t.Width, t.Height)
    MacroD = "'Delete " & i & ", " & j * 7 & "'"
   
    With btn
      .OnAction = MacroD
      .Caption = "<-- Delete Skill "
      .Name = "<-- Delete Skill"
    End With
  Next j
  Next i
  Application.ScreenUpdating = True
End Sub

Sub SearchF()
    
    
    Dim r As Range

   
        
    Set r = ActiveSheet.Range(Cells(5, 3), Cells(5, 180)).Find(ActiveSheet.Range("C3").Value)
    If Not r Is Nothing Then
        
        r.Select
        
        
        Exit Sub
    End If
    Debug.Print "not found"

    MsgBox "Not Found!"
End Sub

Sub DATA()
  Dim btn As Button
  Dim i
  Dim MacroD
  Dim j
  j = 2
  Application.ScreenUpdating = False
  ActiveSheet.Buttons.Delete
  Dim t As Range
  For i = 4 To 90 Step 1
  
    Set t = ActiveSheet.Range(Cells(i, j), Cells(i, j))
    Set btn = ActiveSheet.Buttons.Add(t.Left, t.Top, t.Width, t.Height)
    MacroD = "'DeleteD " & i & ", " & j * 7 & "'"
    With btn
      .OnAction = MacroD
      .Caption = "Delete Skill --> "
      .Name = "Delete Skill -->"
    End With
  
  Next i
  Application.ScreenUpdating = True
End Sub



Sub DeleteD(r, c)
'
' Macro1 Macro
'

'
'    Range(Cells(r, c - 6), Cells(r, c - 2)).Select
'    Selection.Delete Shift:=xlUp
Dim Skill As Variant
Dim Ra As Range
Dim i
    Skill = Cells(r, 4).Value
    Debug.Print Skill
    i = 3
    Set Ra = Worksheets("User skills library").Range("C1:C30").Find(What:=Skill, LookIn:=xlValues) 'Cells(1, i), Cells(30, i)
    Debug.Print Ra.Value
    While Not Ra Is Nothing
        Ra.ClearContents
        Ra.Offset(0, 2).ClearContents
        Ra.Offset(0, 3).ClearContents
        
        
        Set Ra = Worksheets("User skills library").Range("C1:C30").Find(What:=Skill, LookIn:=xlValues)
    Wend
    
    Worksheets("Digital skill Library Data").Range(Cells(r, 3), Cells(r, 10)).Select
    Selection.ClearContents
    
End Sub




