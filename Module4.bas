Attribute VB_Name = "Module4"
Option Explicit

Sub Ppt()

Dim W
Dim H
Dim NumS
Dim Title
W = 430
H = 192
NumS = 1
Dim PptDoc As Object
Dim slideObj As Object
Dim pptLayout As Object
 
    
Worksheets("Job Analysis").Activate

Set PptDoc = GetObject("")

'Set pptLayout = PptDoc.Designs(1).SlideMaster.CustomLayouts(2)
Set pptLayout = PptDoc.Slides(1).CustomLayout
Set slideObj = PptDoc.Slides.AddSlide(1, pptLayout)


Title = ActiveSheet.Range("C6").Value & " --> " & ActiveSheet.Range("C36").Value
With PptDoc
    
    .Slides(NumS).Shapes("Titre 5").TextFrame.TextRange.Text = Title
End With
   
   
ActiveSheet.Shapes("Bonus").Select
ActiveChart.ChartArea.Copy
With PptDoc
'    .Slides(2).Shapes("ShapeJob").TextFrame.TextRange.Text = "Test"
'    .Slides(1).Shapes("GraphTest").Chart.ChartData.Activate
'    .Slides(1).Shapes("GraphTest").Chart.SetSourceData Source:="'Job Analysis'!$C$40:$E$51"
    .Slides(NumS).Shapes.Paste
    .Slides(NumS).Shapes("Bonus").Left = 45
    .Slides(NumS).Shapes("Bonus").Top = 110
    .Slides(NumS).Shapes("Bonus").Height = H
    .Slides(NumS).Shapes("Bonus").Width = W
    
End With

ActiveSheet.Shapes("Perfect Match").Select
ActiveChart.ChartArea.Copy
With PptDoc
'    .Slides(2).Shapes("ShapeJob").TextFrame.TextRange.Text = "Test"
'    .Slides(1).Shapes("GraphTest").Chart.ChartData.Activate
'    .Slides(1).Shapes("GraphTest").Chart.SetSourceData Source:="'Job Analysis'!$C$40:$E$51"
    .Slides(NumS).Shapes.Paste
    .Slides(NumS).Shapes("Perfect Match").Left = 55 + W
    .Slides(NumS).Shapes("Perfect Match").Top = 110
    .Slides(NumS).Shapes("Perfect Match").Height = H
    .Slides(NumS).Shapes("Perfect Match").Width = W
    
End With

ActiveSheet.Shapes("Upskilling").Select
ActiveChart.ChartArea.Copy

With PptDoc
'    .Slides(2).Shapes("ShapeJob").TextFrame.TextRange.Text = "Test"
'    .Slides(1).Shapes("GraphTest").Chart.ChartData.Activate
'    .Slides(1).Shapes("GraphTest").Chart.SetSourceData Source:="'Job Analysis'!$C$40:$E$51"
    .Slides(NumS).Shapes.Paste
    .Slides(NumS).Shapes("Upskilling").Left = 45
    .Slides(NumS).Shapes("Upskilling").Top = 115 + H
    .Slides(NumS).Shapes("Upskilling").Height = H
    .Slides(NumS).Shapes("Upskilling").Width = W
    
End With

ActiveSheet.Shapes("Reskilling").Select
ActiveChart.ChartArea.Copy
With PptDoc
'    .Slides(2).Shapes("ShapeJob").TextFrame.TextRange.Text = "Test"
'    .Slides(1).Shapes("GraphTest").Chart.ChartData.Activate
'    .Slides(1).Shapes("GraphTest").Chart.SetSourceData Source:="'Job Analysis'!$C$40:$E$51"
    .Slides(NumS).Shapes.Paste
    .Slides(NumS).Shapes("Reskilling").Left = 55 + W
    .Slides(NumS).Shapes("Reskilling").Top = 115 + H
    .Slides(NumS).Shapes("Reskilling").Height = H
    .Slides(NumS).Shapes("Reskilling").Width = W
    
End With

End Sub

Sub JobPpt()

Dim W
Dim H
Dim NumS
'Dim Title
Dim JCount
Dim r
Dim Name
Dim JobFamily
r = 5
Dim c
c = 11
W = 337
H = 220
NumS = 4
Dim PptDoc As Object
Dim slideObj As Object
Dim pptLayout As Object
Dim myChart As Object
    
    
'
    Worksheets("User skills library").Activate
    
    Set PptDoc = GetObject("")
    
    Name = ActiveSheet.Cells(r, c).Value
    ActiveSheet.Shapes(Name).Select
    ActiveChart.ChartArea.Copy
    With PptDoc
      
    
        For Each myChart In .Slides(NumS).Shapes
            
            If myChart.Name = Name Then
                
                myChart.Delete
                
            End If
            
        Next myChart
        
        
        .Slides(NumS).Shapes("ShapeJob").TextFrame.TextRange.Text = Name
        .Slides(NumS).Shapes("ShapeJob").Fill.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("OverallMission").Fill.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("OverallMission").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("Rectangle").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("SkillsRequirements").Fill.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("SkillsRequirements").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("JobFamilyH").Fill.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("JobFamilyH").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("Description").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        .Slides(NumS).Shapes("LLegende").Line.ForeColor.ObjectThemeColor = Cells(r, c).Interior.ThemeColor
        
        
        .Slides(NumS).Shapes.Paste
        .Slides(NumS).Shapes(Name).Left = 510
        .Slides(NumS).Shapes(Name).Top = 250
        .Slides(NumS).Shapes(Name).Height = H
        .Slides(NumS).Shapes(Name).Width = W
        
        
        
    
    End With

Set JobFamily = Worksheets("Digital skill library Data").Range("K3:AD3").Find(Name).Offset(-1, 0)

Set JobFamily = JobFamily.MergeArea.Cells(1, 1)
PptDoc.Slides(NumS).Shapes("JobFamilyH").TextFrame.TextRange.Text = "OTHERS: " & UCase(JobFamily.Value)

If JobFamily.Address = "$K$2" Or JobFamily.Address = "$M$2" Then JCount = 1
End If

If JobFamily.Address = "$O$2" Or JobFamily.Address = "$T$2" Then JCount = 4
End If

If JobFamily.Address = "$Y$2" Then JCount = 5
End If


Debug.Print JobFamily.Address
'

End Sub
