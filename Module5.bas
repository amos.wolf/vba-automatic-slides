Attribute VB_Name = "Module5"
Option Explicit
Dim ppMouseClick
Dim ppActionHyperlink
Dim ppPasteEnhancedMetafile
Dim PptDoc As Object
Dim Path
Dim mySlide
Dim NameSlide
Dim TimerCheck
Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As LongPtr)


Sub AllPptDoc()
Dim NameJobs
Dim Job1
Dim Job2
Dim Job
Dim LocJob1
Dim LocJob2
Dim i
Dim j
Dim Shape1
Dim Slide1
ppMouseClick = 1
ppActionHyperlink = 7
ppPasteEnhancedMetafile = 2
Path = ""
TimerCheck = False
Set PptDoc = GetObject(Path)
Application.ScreenUpdating = True


'For Each Slide1 In PptDoc.Slides
'For Each Shape1 In Slide1.Shapes
'Debug.Print Shape1.Name
'If Shape1.Name = "TextBox 24" Then
'Debug.Print "1"
'Slide1.Shapes("TextBox 24").Delete
'End If
'Next Shape1
'Next Slide1



NameJobs = Worksheets("Skills framework").Range("M3:AF3")


SkillDescription

Worksheets("Job skills").Activate
For Each Job In NameJobs


    JobDetails Job
    DoEvents

Next

For Each Job In NameJobs


    Glossary Job
    DoEvents
Next


i = 3
j = 3
For Each mySlide In PptDoc.Slides
On Error Resume Next
If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "SKILL LIST:" Then
    NameSlide = mySlide.Name
    End If
Next

For Each Job1 In NameJobs
i = i + 1
j = 3
    For Each Job2 In NameJobs

    j = j + 1

Job1 = CStr(Job1)
Job2 = CStr(Job2)
Debug.Print Job1
Debug.Print Job2

        If i <> j And Worksheets("Objective").Cells(j, i).Value < 3 Then
            SkillList Job1, Job2
            DoEvents

        End If

    Next
Next


AddLinkJobDetails
OverviewLink

Application.ScreenUpdating = True
End Sub

Sub SkillList(Job1, Job2)

Dim newSlide
Dim NPerfectMatch


Dim i
Dim newShape
Dim NameSkill
Dim LocSkill
Dim List
Dim where
Dim typeSkill
Dim NameSlide1


Worksheets("Mobility dashboard").Range("C6") = Job1

Worksheets("Mobility dashboard").Range("C36") = Job2

Set newSlide = PptDoc.Slides(NameSlide).Duplicate
newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "SKILL LIST: " & Worksheets("Mobility dashboard").Range("C6") & " - " & Worksheets("Mobility dashboard").Range("C36")
newSlide.Shapes("Job1").TextFrame.TextRange.Text = Worksheets("Mobility dashboard").Range("C6")
newSlide.Shapes("Job1").Fill.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("Job1").TextFrame.TextRange.Text).Fill.ForeColor
newSlide.Shapes("Job1").Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("Job1").TextFrame.TextRange.Text).Line.ForeColor
newSlide.Shapes("Job2").TextFrame.TextRange.Text = Worksheets("Mobility dashboard").Range("C36")
newSlide.Shapes("Job2").Fill.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("Job2").TextFrame.TextRange.Text).Fill.ForeColor
newSlide.Shapes("Job2").Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("Job2").TextFrame.TextRange.Text).Line.ForeColor
List = Array("Perfect Match", "Upskilling", "Reskilling")
where = 190
If Job1 = "E-merchandising & eventing - Indirect" Or Job2 = "E-merchandising & eventing - Indirect" Then
Debug.Print Worksheets("Mobility dashboard").Range("C36")
Debug.Print Worksheets("Mobility dashboard").Range("C6")
End If

For Each typeSkill In List
NPerfectMatch = Application.WorksheetFunction.CountIf(Worksheets("Mobility dashboard").Range("D40:D100"), typeSkill)
If Job1 = "E-merchandising & eventing - Indirect" Or Job2 = "E-merchandising & eventing - Indirect" Then
Debug.Print NPerfectMatch
End If

If NPerfectMatch <> 0 Then


    Set LocSkill = Worksheets("Mobility dashboard").Range("D39:D100").Find(What:=typeSkill, LookIn:=xlValues)
    
    NameSkill = LocSkill.Offset(0, -1)
    
    For i = 1 To NPerfectMatch
        
        
        Set newShape = newSlide.Shapes(typeSkill).Duplicate
        
        If i = 1 Then
            newSlide.Shapes(typeSkill).Delete
        End If
        If typeSkill = "Upskilling" Then
        where = 780
        End If
        If typeSkill = "Reskilling" Then
        where = 1370
        End If
        newShape.Left = where
        newShape.Top = 310 + 45 * i
        newShape.TextFrame.TextRange.Text = NameSkill
        
        With newShape.ActionSettings(ppMouseClick)
            .Action = ppActionHyperlink
            .Hyperlink.SubAddress = "Description: " & NameSkill
        End With
        
        Set LocSkill = Worksheets("Mobility dashboard").Range(LocSkill.Address & ":D100").Find(What:=typeSkill, LookIn:=xlValues)
        NameSkill = LocSkill.Offset(0, -1)
        DoEvents
    Next

Else
newSlide.Shapes(typeSkill).Delete
End If

Next typeSkill


With newSlide.Shapes("Link").ActionSettings(ppMouseClick)
    .Action = ppActionHyperlink
    .Hyperlink.SubAddress = "JOB ROLE DETAIL: " & Job1
End With

TimerCheck = True
End Sub
Sub SkillQuestions(Job1, Job2, NameSlide1)

Dim newSlide
Dim NPerfectMatch


Dim i
Dim newShape
Dim NameSkill
Dim LocSkill
Dim List
Dim where
Dim typeSkill
Dim img


For Each mySlide In PptDoc.Slides
On Error Resume Next
If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "QUESTIONS:" Then
    NameSlide = mySlide.Name
    End If
Next

Set newSlide = PptDoc.Slides(NameSlide).Duplicate

newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "QUESTIONS: " & Job1 & " - " & Job2

PptDoc.Slides(NameSlide1).Shapes("SkillList").Copy

Set img = newSlide.Shapes.PasteSpecial(ppPasteEnhancedMetafile)
With img
    .Top = 100
    .Left = 480
    .Width = 1000
 
End With
    
End Sub

Sub JobDetails(Job)

Dim newSlide
Dim NMobility


Dim i
Dim j
Dim newShape
Dim NameSkill
Dim LocSkill
Dim List
Dim where
Dim typeSkill
Dim Mobility
Dim LocJob
Dim JobName
Dim k
Dim Shape1
Dim img

For Each mySlide In PptDoc.Slides
On Error Resume Next
If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "JOB ROLE DETAIL:" Then
    NameSlide = mySlide.Name
    End If
Next

Set newSlide = PptDoc.Slides(NameSlide).Duplicate

newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "JOB ROLE DETAIL: " & Job
newSlide.Shapes("NameJob").TextFrame.TextRange.Text = Job
newSlide.Shapes("DescriptionJob").TextFrame.TextRange.Text = Worksheets("Mission").Range("C2:C1000").Find(What:=Job, LookIn:=xlValues).Offset(0, 1).Value


Worksheets("Job skills").Shapes(Job).Select
ActiveChart.ChartArea.Copy
Debug.Print newSlide.Shapes("NameJob").TextFrame.TextRange.Text

With newSlide
    .Shapes.Paste
    .Shapes(Job).Left = 260
    .Shapes(Job).Top = 370
    .Shapes(Job).Height = 550
    .Shapes(Job).Width = 600
    .Shapes(Job).SendToBack
    .Shapes(Job).Chart.FullSeriesCollection(1).Format.Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
    .Shapes(Job).Chart.ChartGroups(1).RadarAxisLabels.Font.Size = 17
End With
newSlide.Shapes(Job).Chart.ChartArea.Copy
Set img = newSlide.Shapes.PasteSpecial(5)
img.Left = 260
img.Top = 370
img.Height = 550
img.Width = 600
img.ZOrder (1)

newSlide.Shapes(Job).Delete
For i = 1 To 2
k = 0

    Mobility = "SecondaryMobility"
    If i = 1 Then
    Mobility = "PrimaryMobility"
    End If
    Set LocSkill = Worksheets("Objective").Range("B3:X3").Find(What:=Job, LookIn:=xlValues)
   
    NMobility = Application.WorksheetFunction.CountIf(Worksheets("Objective").Range(LocSkill.Address & ":" & LocSkill.Offset(100, 0).Address), i)
   If NMobility <> 0 Then
    For j = 1 To NMobility
        Set newShape = newSlide.Shapes(Mobility).Duplicate
        If j = 1 Then
            newSlide.Shapes(Mobility).Delete
        End If
        where = 1335
        If Mobility = "PrimaryMobility" Then
        where = 997
        End If
        
        newShape.Left = where
        newShape.Top = 405 + 61 * j
      
        For k = k + 1 To 100
            If Worksheets("Objective").Cells(k, LocSkill.Column) = i Then
                JobName = Worksheets("Objective").Cells(k, 3).Value
                Exit For
            End If
        Next

        newShape.TextFrame.TextRange.Text = JobName
        
    Next
    Else
    newSlide.Shapes(Mobility).Delete
    End If




Next

'ColorJobDetails

For Each Shape1 In newSlide.Shapes
            
            
            If Shape1.Name = "PrimaryMobility" Or Shape1.Name = "SecondaryMobility" Then
                Shape1.Fill.ForeColor = PptDoc.Slides(5).Shapes(Shape1.TextFrame.TextRange.Text).Fill.ForeColor
                Shape1.Line.ForeColor = PptDoc.Slides(5).Shapes(Shape1.TextFrame.TextRange.Text).Line.ForeColor
               
            End If
            If Shape1.Name = "Requirement" Or Shape1.Name = "Mission" Or Shape1.Name = "NameJob" Or Shape1.Name = "Pathway" Then
                Shape1.Fill.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
                Shape1.Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Line.ForeColor
            End If
            If Shape1.Name = "Background" Or Shape1.Name = "DescriptionJob" Or Shape1.Name = "GlobalOverview" Then
                
                Shape1.Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
            End If

Next







TimerCheck = True
End Sub

Sub Glossary(Job)

Dim newSlide
Dim NPerfectMatch


Dim i
Dim newShape
Dim NameSkill
Dim LocJob
Dim List
Dim where
Dim typeSkill
Dim j
Dim Table1
Dim rng


For Each mySlide In PptDoc.Slides
On Error Resume Next
If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "GLOSSARY SKILLS:" Then
    NameSlide = mySlide.Name
    End If
Next

Set newSlide = PptDoc.Slides(NameSlide).Duplicate
newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "GLOSSARY SKILLS: " & Job

newSlide.Shapes("NameJob").TextFrame.TextRange.Text = Job
newSlide.Shapes("NameJob").Fill.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
newSlide.Shapes("NameJob").Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Line.ForeColor
newSlide.Shapes("Background").Line.ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor

Set LocJob = Worksheets("Job skills").Range("C5:EK5").Find(What:=Job, LookIn:=xlValues).Offset(2, 0)

       
Worksheets("Job skills").Range(LocJob.Address & ":" & LocJob.Offset(0, 1).Address).Select

Worksheets("Job skills").Range(Selection, Selection.End(xlDown)).Copy

Set Table1 = newSlide.Shapes.Paste
With Table1
    .Table.Columns(1).Width = 500
    .Table.Columns(2).Width = 1000
    .Top = 225
    .Left = -555
    .Height = 700
    '.Width = 1500
    
End With
DoEvents
For i = 1 To Table1.Table.Columns.Count
For j = 1 To Table1.Table.Rows.Count
Table1.Table.Cell(j, i).Shape.TextFrame.TextRange.Font.Size = 16
Table1.Table.Cell(j, 1).Shape.TextFrame.TextRange.Font.Bold = True
Table1.Table.Cell(j, i).Borders(1).ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
Table1.Table.Cell(j, i).Borders(3).ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Fill.ForeColor
Table1.Table.Cell(j, i).Borders(2).ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Line.ForeColor
Table1.Table.Cell(j, i).Borders(4).ForeColor = PptDoc.Slides(5).Shapes(newSlide.Shapes("NameJob").TextFrame.TextRange.Text).Line.ForeColor
Next j
Next i
DoEvents
With newSlide.Shapes("Link").ActionSettings(ppMouseClick)
    .Action = ppActionHyperlink
    .Hyperlink.SubAddress = "JOB ROLE DETAIL: " & Job
End With
TimerCheck = True
End Sub
Function IsInArray(stringToBeFound As String, arr As Variant) As Boolean
  IsInArray = (UBound(Filter(arr, stringToBeFound)) > -1)
End Function
Sub AddLinkJobDetails()


Dim NameJobs
Dim Job1
Dim Job2
Dim Job
ppMouseClick = 1
ppActionHyperlink = 7
ppPasteEnhancedMetafile = 2

Application.ScreenUpdating = True

NameJobs = Worksheets("Mobilities overview").Range("D3:W3")

Dim newSlide
Dim NPerfectMatch


Dim i
Dim newShape
Dim NameSkill
Dim LocJob
Dim List
Dim where
Dim typeSkill
Dim j
Dim Table1
Dim rng
Dim Shape1

For Each Job In NameJobs
Job = CStr(Job)

For Each mySlide In PptDoc.Slides
    
    On Error Resume Next
    If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "JOB ROLE DETAIL: " & Job Then
        
        Set newSlide = mySlide
      
    End If
    
Next


    For Each Shape1 In newSlide.Shapes
 
        If Shape1.TextFrame.TextRange.Text = "GLOSSARY SKILLS" Then
            
            With Shape1.ActionSettings(ppMouseClick)
                .Action = ppActionHyperlink
                .Hyperlink.SubAddress = "GLOSSARY SKILLS: " & Job
            End With
            
        End If

        If Shape1.Name = "PrimaryMobility" Or Shape1.Name = "SecondaryMobility" Then
            
            With Shape1.ActionSettings(ppMouseClick)
                .Action = ppActionHyperlink
                .Hyperlink.SubAddress = "SKILL LIST: " & Job & " - " & Shape1.TextFrame.TextRange.Text
            End With
            
        End If

        DoEvents
    Next
DoEvents
Next



End Sub
Sub OverviewLink()
Dim Shape1
ppMouseClick = 1
ppActionHyperlink = 7

For Each Shape1 In PptDoc.Slides(5).Shapes
Debug.Print PptDoc.Slides(5).Shapes("Titre 2").TextFrame.TextRange.Text
Debug.Print Shape1.Name
    
    If Shape1.Name = Shape1.TextFrame.TextRange.Text Then
            With Shape1.ActionSettings(ppMouseClick)
                .Action = ppActionHyperlink
                .Hyperlink.SubAddress = "JOB ROLE DETAIL: " & Shape1.TextFrame.TextRange.Text
            End With
            End If

Next
End Sub
Sub SkillDescription()

Dim newSlide
Dim NPerfectMatch


Dim i
Dim newShape
Dim NameSkill
Dim LocJob
Dim List
Dim where
Dim typeSkill
Dim j
Dim Table1
Dim rng


For Each mySlide In PptDoc.Slides
On Error Resume Next
If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "DESCRIPTION:" Then
    NameSlide = mySlide.Name
    End If
Next


For i = 4 To 150

    If Worksheets("Skills framework").Cells(i, 4).Value <> "" Then
        Set newSlide = PptDoc.Slides(NameSlide).Duplicate
        newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "Description: " & Worksheets("Skills framework").Cells(i, 4).Value
        newSlide.Shapes("NameSkill").TextFrame.TextRange.Text = Worksheets("Skills framework").Cells(i, 4).Value
        newSlide.Shapes("DescriptionSkill").TextFrame.TextRange.Text = Worksheets("Skills framework").Cells(i, 4 + 2).Value
        
        
    End If
Next
TimerCheck = True
End Sub

Sub HROverview(Job1, Job2)
'Dim PptDoc As Object
'Dim Path
'Dim newSlide
'Dim NPerfectMatch
'Dim mySlide
'Dim NameSlide
'Dim i
'Dim newShape
'Dim NameSkill
'Dim LocSkill
'Dim List
'Dim where
'Dim typeSkill
'Dim NameSlide1
'Path = ""
'
'Set PptDoc = GetObject(Path)
'Worksheets("Mobility dashboard").Activate
'ActiveSheet.Range("C6") = Job1
''ActiveSheet.Range("C6") = "Head of CRM "
'ActiveSheet.Range("C36") = Job2
''ActiveSheet.Range("C36") = "CRM platform & performance"
'
'
''PptDoc.Slides(8).Name = "Title1"
''Debug.Print PptDoc.Slides(8).Name
'
'For Each mySlide In PptDoc.Slides
'On Error Resume Next
'If mySlide.Shapes("Titre 2").TextFrame.TextRange.Text = "SKILL LIST:" Then
'    NameSlide = mySlide.Name
'    End If
'Next
'
'
'Set newSlide = PptDoc.Slides(NameSlide).Duplicate
'newSlide.Shapes("Titre 2").TextFrame.TextRange.Text = "SKILL LIST: " & ActiveSheet.Range("C6") & " - " & ActiveSheet.Range("C36")
'newSlide.Shapes("Job1").TextFrame.TextRange.Text = ActiveSheet.Range("C6")
'newSlide.Shapes("Job2").TextFrame.TextRange.Text = ActiveSheet.Range("C36")
'List = Array("Perfect Match", "Upskilling", "Reskilling")
End Sub

