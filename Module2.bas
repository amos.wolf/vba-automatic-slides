Attribute VB_Name = "Module2"
Option Explicit

Sub PerfectMatch()
Attribute PerfectMatch.VB_ProcData.VB_Invoke_Func = " \n14"
'
' Macro1 Macro
'

'
Dim i
i = 0
Dim Test As Range
    Dim LocC
    LocC = ""
    
    Dim LocT
    LocT = ""
    
    Dim Skills
    Skills = ""
    Dim c As Range
    Dim f
    f = "Perfect Match"
    Range("D39:D70").Select
    Set c = Cells.Find(What:=f, After:=ActiveCell, LookIn:=xlValues, _
            lookat:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
            MatchCase:=True, SearchFormat:=False)
    If c.Row < 30 Then
        Exit Sub
        End If
        
            
            Do
            i = i + 1
                If LocC <> "" Then
                    LocC = LocC & ","
                    End If
                If LocT <> "" Then
                    LocT = LocT & ","
                    End If
                If Skills <> "" Then
                    Skills = Skills & ","
                    End If
                LocC = LocC & "'Mobility dashboard'!" & c.Offset(, 1).Address
                LocT = LocT & "'Mobility dashboard'!" & c.Offset(, 2).Address
                Skills = Skills & "'Mobility dashboard'!" & c.Offset(, -1).Address
                Set Test = c
                Set c = Cells.FindNext(After:=c)
                
                
                
            Loop While c = f And i < 30 And Test.Row < c.Row
       
    Debug.Print LocT
    Range("C40").Select

    Range("C40:F70").Select
    ActiveSheet.Shapes.AddChart2(317, xlRadarMarkers).Select
    ActiveChart.SetSourceData Source:=Range("'Mobility dashboard'!$C$40:$F$70")
    Application.CutCopyMode = False
    ActiveChart.FullSeriesCollection(1).Values = "=" & LocC
    ActiveChart.FullSeriesCollection(2).Values = "=" & LocT
    ActiveChart.FullSeriesCollection(1).XValues = "=" & Skills
    ActiveChart.FullSeriesCollection(1).Name = "='Mobility dashboard'!$C$6"
    ActiveChart.FullSeriesCollection(2).Name = "='Mobility dashboard'!$C$36"
    ActiveChart.ChartTitle.Text = f
    ActiveChart.Parent.Name = f
    
    ActiveChart.FullSeriesCollection(1).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
    With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(0, 70, 196)
    Selection.MarkerBackgroundColor = RGB(0, 70, 196)
    
   
    
    ActiveChart.FullSeriesCollection(2).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
     With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(223, 95, 31)
    Selection.MarkerBackgroundColor = RGB(223, 95, 31)
        
   
    


   
   
End Sub

Sub Reskilling()
'
' Macro1 Macro
'

'
Dim i
i = 0
Dim Test As Range
    Dim LocC
    LocC = ""
    
    Dim LocT
    LocT = ""
    
    Dim Skills
    Skills = ""
    Dim c As Range
    Dim f
    f = "Reskilling"
    Range("D39:D70").Select
    Set c = Cells.Find(What:=f, After:=ActiveCell, LookIn:=xlValues, _
            lookat:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
            MatchCase:=True, SearchFormat:=False)
     If c.Row < 30 Then
        Exit Sub
        End If
        
            'Set c = Cells.FindNext(After:=ActiveCell)
            Do
            i = i + 1
                If LocC <> "" Then
                    LocC = LocC & ","
                    End If
                If LocT <> "" Then
                    LocT = LocT & ","
                    End If
                If Skills <> "" Then
                    Skills = Skills & ","
                    End If
                LocC = LocC & "'Mobility dashboard'!" & c.Offset(, 1).Address
                LocT = LocT & "'Mobility dashboard'!" & c.Offset(, 2).Address
                Skills = Skills & "'Mobility dashboard'!" & c.Offset(, -1).Address
                Set Test = c
                Set c = Cells.FindNext(After:=c)
                
            Loop While c = f And i < 30 And Test.Row < c.Row
       
    
    Range("C40").Select

    Range("C40:F70").Select
    ActiveSheet.Shapes.AddChart2(317, xlRadarMarkers).Select
    ActiveChart.SetSourceData Source:=Range("'Mobility dashboard'!$C$40:$F$70")
    Application.CutCopyMode = False
    ActiveChart.FullSeriesCollection(1).Values = "=" & LocC
    ActiveChart.FullSeriesCollection(2).Values = "=" & LocT
    ActiveChart.FullSeriesCollection(1).XValues = "=" & Skills
    ActiveChart.FullSeriesCollection(1).Name = "='Mobility dashboard'!$C$6"
    ActiveChart.FullSeriesCollection(2).Name = "='Mobility dashboard'!$C$36"
    ActiveChart.ChartTitle.Text = f
    ActiveChart.Parent.Name = f
    
    ActiveChart.FullSeriesCollection(1).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
    With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(0, 70, 196)
    Selection.MarkerBackgroundColor = RGB(0, 70, 196)
    
   
    
    ActiveChart.FullSeriesCollection(2).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
     With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(223, 95, 31)
    Selection.MarkerBackgroundColor = RGB(223, 95, 31)
    


   
   
End Sub

Sub Upskilling()
'
' Macro1 Macro
'

'
Dim i
i = 0
Dim Test As Range
    Dim LocC
    LocC = ""
    
    Dim LocT
    LocT = ""
    
    Dim Skills
    Skills = ""
    Dim c As Range
    Dim f
    f = "Upskilling"
    Range("D39:D70").Select
    Set c = Cells.Find(What:=f, After:=ActiveCell, LookIn:=xlValues, _
            lookat:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
            MatchCase:=True, SearchFormat:=False)
     If c Is Nothing Then
        Exit Sub
        End If
        
            Do
            i = i + 1
                If LocC <> "" Then
                    LocC = LocC & ","
                    End If
                If LocT <> "" Then
                    LocT = LocT & ","
                    End If
                If Skills <> "" Then
                    Skills = Skills & ","
                    End If
                LocC = LocC & "'Mobility dashboard'!" & c.Offset(, 1).Address
                LocT = LocT & "'Mobility dashboard'!" & c.Offset(, 2).Address
                Skills = Skills & "'Mobility dashboard'!" & c.Offset(, -1).Address
                Set Test = c
                Set c = Cells.FindNext(After:=c)
                
            Loop While c = f And i < 30 And Test.Row < c.Row
       
    
    Range("C40").Select

    Range("C40:F70").Select
    ActiveSheet.Shapes.AddChart2(317, xlRadarMarkers).Select
    ActiveChart.SetSourceData Source:=Range("'Mobility dashboard'!$C$40:$F$70")
    Application.CutCopyMode = False
    ActiveChart.FullSeriesCollection(1).Values = "=" & LocC
    ActiveChart.FullSeriesCollection(2).Values = "=" & LocT
    ActiveChart.FullSeriesCollection(1).XValues = "=" & Skills
    ActiveChart.FullSeriesCollection(1).Name = "='Mobility dashboard'!$C$6"
    ActiveChart.FullSeriesCollection(2).Name = "='Mobility dashboard'!$C$36"
    ActiveChart.ChartTitle.Text = f
    ActiveChart.Parent.Name = f

    ActiveChart.FullSeriesCollection(1).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
    With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(0, 70, 196)
    Selection.MarkerBackgroundColor = RGB(0, 70, 196)
    
   
    
    ActiveChart.FullSeriesCollection(2).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
     With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(223, 95, 31)
    Selection.MarkerBackgroundColor = RGB(223, 95, 31)
   
   
End Sub


Sub Bonus()
'
' Macro1 Macro
'

'
Dim i
i = 0
Dim Test As Range
    Dim LocC
    LocC = ""
    
    Dim LocT
    LocT = ""
    
    Dim Skills
    Skills = ""
    Dim c As Range
    Dim f
    f = "Bonus"
    Range("D39:D70").Select
    Set c = Cells.Find(What:=f, After:=ActiveCell, LookIn:=xlValues, _
            lookat:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
            MatchCase:=True, SearchFormat:=False)
     If c.Row < 30 Then
        Exit Sub
        End If
        
            Do
            i = i + 1
                If LocC <> "" Then
                    LocC = LocC & ","
                    End If
                If LocT <> "" Then
                    LocT = LocT & ","
                    End If
                If Skills <> "" Then
                    Skills = Skills & ","
                    End If
                LocC = LocC & "'Mobility dashboard'!" & c.Offset(, 1).Address
                LocT = LocT & "'Mobility dashboard'!" & c.Offset(, 2).Address
                Skills = Skills & "'Mobility dashboard'!" & c.Offset(, -1).Address
                Set Test = c
                Set c = Cells.FindNext(After:=c)
                
            Loop While c = f And i < 30 And Test.Row < c.Row
       
    
    Range("C40").Select

    Range("C40:F70").Select
    ActiveSheet.Shapes.AddChart2(317, xlRadarMarkers).Select
    ActiveChart.SetSourceData Source:=Range("'Mobility dashboard'!$C$40:$F$70")
    Application.CutCopyMode = False
   ActiveChart.FullSeriesCollection(1).Values = "=" & LocC
    ActiveChart.FullSeriesCollection(2).Values = "=" & LocT
    ActiveChart.FullSeriesCollection(1).XValues = "=" & Skills
    ActiveChart.FullSeriesCollection(1).Name = "='Mobility dashboard'!$C$6"
    ActiveChart.FullSeriesCollection(2).Name = "='Mobility dashboard'!$C$36"
    ActiveChart.ChartTitle.Text = f
    ActiveChart.Parent.Name = f
    
    
    ActiveChart.FullSeriesCollection(1).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
    With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent3
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(0, 70, 196)
    Selection.MarkerBackgroundColor = RGB(0, 70, 196)
    
   
    
    ActiveChart.FullSeriesCollection(2).Select
    With Selection.Format.Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .Transparency = 0
        .Solid
    End With
     With Selection.Format.Line
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent5
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0
        .Transparency = 0
    End With
    Selection.MarkerSize = 10
    Selection.MarkerForegroundColor = RGB(223, 95, 31)
    Selection.MarkerBackgroundColor = RGB(223, 95, 31)

   
   
End Sub


