Attribute VB_Name = "Module1"
Option Explicit

Sub ALLGraphs()
Application.ScreenUpdating = False
ActiveSheet.Unprotect
Dim H
Dim W

H = 280
W = 400

Dim myChart As ChartObject
For Each myChart In ActiveSheet.ChartObjects

If myChart.Name = "Bonus" Or myChart.Name = "Upskilling" Or myChart.Name = "Reskilling" Or myChart.Name = "Perfect Match" Then
    myChart.Delete
End If
Next myChart


Dim PerfectName




If Not Range("D40:D70").Find(What:="Bonus") Is Nothing Then
Dim BonusName
    Bonus
    BonusName = ActiveChart.Name
    Debug.Print BonusName
    BonusName = Replace(BonusName, "Mobility dashboard ", "")
    
   With ActiveSheet.Shapes(BonusName)
       .Left = Range("I39").Left
       .Top = Range("I39").Top
       .Height = H
       .Width = W
    End With
    
    With ActiveSheet.Shapes(BonusName).Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent1
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0.95
        .BackColor.ObjectThemeColor = msoThemeColorBackground1
        .BackColor.TintAndShade = 0
        .BackColor.Brightness = 0
        .Patterned msoPattern50Percent
    End With
    ActiveSheet.Shapes(BonusName).Select
    ActiveChart.ChartTitle.Font.Color = RGB(0, 0, 150)
    ActiveChart.Axes(xlValue).MajorUnit = 1
    
    End If
    

If Not Range("D40:D70").Find(What:="Perfect Match") Is Nothing Then
    PerfectMatch
    PerfectName = ActiveChart.Name
    PerfectName = Replace(PerfectName, "Mobility dashboard ", "")
    With ActiveSheet.Shapes(PerfectName)
       .Left = Range("Q39").Left
       .Top = Range("Q39").Top
       .Height = H
       .Width = W
    End With
    
     With ActiveSheet.Shapes(PerfectName).Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent2
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0.9
        .BackColor.ObjectThemeColor = msoThemeColorBackground1
        .BackColor.TintAndShade = 0
        .BackColor.Brightness = 0
        .Patterned msoPattern50Percent
    End With
    ActiveSheet.Shapes(PerfectName).Select
    ActiveChart.ChartTitle.Font.Color = RGB(0, 100, 0)
    ActiveChart.Axes(xlValue).MajorUnit = 1
    
    End If
    


If Not Range("D40:D70").Find(What:="Upskilling") Is Nothing Then
    
    Dim UpskillingName
    Upskilling
    UpskillingName = ActiveChart.Name
    UpskillingName = Replace(UpskillingName, "Mobility dashboard ", "")
    With ActiveSheet.Shapes(UpskillingName)
       .Left = Range("I46").Left
       .Top = Range("I46").Top
       .Height = H
       .Width = W
    End With
    
    With ActiveSheet.Shapes(UpskillingName).Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent6
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0.95
        .BackColor.ObjectThemeColor = msoThemeColorBackground1
        .BackColor.TintAndShade = 0
        .BackColor.Brightness = 0
        .Patterned msoPattern50Percent
    End With
    ActiveSheet.Shapes(UpskillingName).Select
    ActiveChart.ChartTitle.Font.Color = RGB(172, 142, 0)
    ActiveChart.Axes(xlValue).MajorUnit = 1
    
    End If
    




If Not Range("D40:D70").Find(What:="Reskilling") Is Nothing Then
Dim ReskillingName
    Reskilling
    ReskillingName = ActiveChart.Name
    ReskillingName = Replace(ReskillingName, "Mobility dashboard ", "")
    With ActiveSheet.Shapes(ReskillingName)
       .Left = Range("Q46").Left
       .Top = Range("Q46").Top
       .Height = H
       .Width = W
    End With

    
    

    With ActiveSheet.Shapes(ReskillingName).Fill
        .Visible = msoTrue
        .ForeColor.ObjectThemeColor = msoThemeColorAccent4
        .ForeColor.TintAndShade = 0
        .ForeColor.Brightness = 0.94
        .BackColor.ObjectThemeColor = msoThemeColorBackground1
        .BackColor.TintAndShade = 0
        .BackColor.Brightness = 0
        .Patterned msoPattern50Percent
    End With
    ActiveSheet.Shapes(ReskillingName).Select
    ActiveChart.ChartTitle.Font.Color = RGB(150, 0, 0)
    ActiveChart.Axes(xlValue).MajorUnit = 1
    
    End If


    
   



   ActiveSheet.Protect DrawingObjects:=False, Contents:=True, Scenarios:= _
        False, AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True
Application.ScreenUpdating = True
End Sub


Sub GraphJobs()
'
' Macro1 Macro
'

'
Application.ScreenUpdating = False
ActiveSheet.Unprotect

Dim c
c = 4
Dim r
r = 7
Dim ns
ns = 24
Dim last
Dim i
Dim j

Dim myChart As ChartObject



For c = 4 To 137 Step 7

    For Each myChart In ActiveSheet.ChartObjects
        If myChart.Name = Cells(r - 2, c).Value Then
            myChart.Delete
        End If
        
    Next myChart
    
Next c


For c = 4 To 137 Step 7
    Set last = Range(Cells(r, c), Cells(r + ns, c)).Find("")
        'Range(Cells(r, c), Cells(r, c + 2)).Select
        'Range(Selection, Selection.End(xlDown)).Select
        'ActiveSheet.Shapes.AddChart2(317, xlRadar).Select
        For i = 0 To 30 Step 1
        
            For j = 1 To 30 Step 1

                If last.Offset(j, 0).Value <> "" Then
                    last.Value = last.Offset(j, 0).Value
                    last.Offset(j, 0).ClearContents
                    last.Offset(0, 2).Value = last.Offset(j, 2).Value
                    last.Offset(j, 2).ClearContents
                    last.Offset(0, 3).Value = last.Offset(j, 3).Value
                    last.Offset(j, 3).ClearContents
                    Exit For
                End If
            Next j
            
            Set last = Range(Cells(r, c), Cells(r + ns, c)).FindNext(After:=last)
                
            
        Next i
    Set last = Range(Cells(r, c), Cells(r + ns, c)).Find("")
    ActiveSheet.Shapes.AddChart2(317, xlRadar).Select
    ActiveChart.SetSourceData Source:=Range("'Job skills'!" & Cells(r, c).Address & ":" & last.Offset(-1, 2).Address)
    ActiveChart.FullSeriesCollection(1).XValues = "='Job skills'!" & Cells(r, c).Address & ":" & last.Offset(-1, 0).Address
    ActiveChart.Parent.Name = Cells(r - 2, c).Value
    ActiveChart.ChartTitle.Delete
    ActiveChart.Axes(xlValue).MajorUnit = 1
    ActiveChart.ChartGroups(1).RadarAxisLabels.Font.Size = 16
    
    
    With ActiveSheet.Shapes(Cells(r - 2, c).Value)
       .Left = Cells(r + 25, c).Left + 0.25 * c
       .Top = Cells(r + 25, c).Top
       .Height = 300
       .Width = 450
    End With
    
    ActiveChart.FullSeriesCollection(1).Format.Line.ForeColor.ObjectThemeColor = Cells(r - 2, c).Interior.ThemeColor
Next c

ActiveSheet.Protect DrawingObjects:=False, Contents:=True, Scenarios:= _
        False, AllowFormattingCells:=True, AllowFormattingColumns:=True, _
        AllowFormattingRows:=True, AllowSorting:=True, AllowFiltering:=True
Application.ScreenUpdating = True
End Sub

